import requests
from bs4 import BeautifulSoup
import enchant
d = enchant.Dict("en_US")

class scrapper:

  def __init__(self,url):
    self.url = url
    page = requests.get(url)
    self.soup = BeautifulSoup(page.content, "html.parser").findAll(text=True)
    self.english_words = []
    self.non_english = []
    self.words = []
    self.unique = {}

    """removes symbols from a list of words
      @returns: a list of words without symbols
    """
  def remove_symbols(self):
    symbol_free = []
    for word in self.soup:
          symbols = "!@#$%^&*()_-+={[}]|\;:\"<>?/.,—" 
          for i in range(len(symbols)):
              word = word.replace(symbols[i], '')
          if len(word) > 0:
              symbol_free.append(word)
    self.soup = symbol_free
    return self.soup

    """removes any formatting the list of words has
       it calls remove_symbols()
    """
  def remove_formatting(self):
    format_free = []
    symbol_free = self.remove_symbols()
    for word in symbol_free:
            formatters= ['\n','\t','\r','div'] 
            for i in range(len(formatters)):
                word = word.replace(formatters[i], '')
            if len(word) > 0:
                format_free.append(word)
    self.soup=format_free

    """splits any sentences in self.soup into words
      it calls remove_formatting()
      @returns: a list of words
    """
  def get_words(self):
    word_list = []
    self.remove_formatting()
    for sentence in self.soup:
      word_list.extend(sentence.split())
    self.words = word_list
    self.soup = word_list

    """check if the word is an english word
       populates english_words list with english words
       populates non_english list with non english words
    """
  def get_english_words(self):
    for name in self.soup:
      if d.check(name):
        self.english_words.append(name.lower())
      else: self.non_english.append(name.lower())
      
  ## get unique words 
    """gets unique words
       @returns a sorted dictionary of unique words on the page
    """
  def get_unique(self):
    for w in sorted(set(self.english_words)):
      self.unique[w]= self.english_words.count(w) 
    return self.unique

  def compare_sites(self,firstSet,siteURLs):
    setList =[]
    result = firstSet
    for url in siteURLs:
      scrap = scrapper(url)
      scrap.get_words()
      setList.append(set(scrap.get_unique()))
    for words in setList:
      result = result.difference(words)
      
    return result