# web-scrapper

Pesapal problem #3 attempt

Write an application which, when given a web page will download the text on it and output a sorted list of the unique words on the page, with counts of the occurrences.

**Extensions**

Consider extending the application to work with a dictionary, or configurable word list, so one can e.g. find the non-English words on the page. Or perhaps provide a way to compare two (or more pages) in terms of words found in both and only in one or the other?

## Overview
* Technology used
* Setup
* Files
* Proposed solution
* Results
* Challenges encountered
* Future modifications

## Technology used
* Python
    * [BeautifulSoup](https://beautiful-soup-4.readthedocs.io/en/latest/)
    * [requests](https://docs.python-requests.org/en/latest/)
    * [enchant](https://pypi.org/project/pyenchant/)

## Setup
* Installs
    - `pip install beautifulsoup4`
    - `pip install requests`
    - `pip install pyenchant`
* Run
    - Main.py
## Files
* Main.py
    - This is the entry file
* Scrapper.py
    - Contains a scrapper class with functions used to scrap
## Proposed solution
The proposed solution intends to a scrapping library to get the words in a page. 
The library however does not provide clean text so we will use some cleaning methods like
removing any formating and symbols. We then split any sentences into individual words.

After getting the result as a list of words we use the enchant library to chack if a word is 
an english word. This is done by checking the spelling of the word. Other alternatives to this 
approach include using the python NLTK word document to check if the word is inside the document.
This method, however is very costly.

Once we have the english and non-english words, we can use the set function to extract unique words 
in the list. Get the count simulteneously and create a dictionary of the unique words and their count.

To compare the words in our site with any other site(s):

1. We first get the urls of the desired sites from the user
2. We then reuse the dynamic functions in scrapper class to get the unique words on the site
3. `result = (SetA-SetN)-setN+1` is the formula we use to get the words in site A that are not in the provided urls

## Results

* Url prompt
![Screenshot](images/unique_word_count.png)

* User menu
![Screenshot](images/user_menu.png)

* Compare sites
![Screenshot](images/compare_sites.png)

## Challenges encountered
* BeautifulSoup
  - The library sometimes extracts meta data from the site
  - It sometimes concatenates words which makes it hard to split english words
  - Some of the words extracted are actual numbers ( enchant considers them as correct english words)

## Future modifications
* Splitting concatenated words
* Better user input prompt handling ( Have a class that manages user input)

