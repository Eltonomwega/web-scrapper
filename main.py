from scrapper import *

# "https://pesapal.freshteam.com/jobs/-z8xM8RCgTx7/junior-developer"

all_words = []
english_words = []
non_english = []
unique = {}

URL = input("Enter page URL: \n")

"""
default behavior (get sorted list of the unique words on the page, with counts of the occurrences.)
"""
scrap = scrapper(URL)

all_words = scrap.get_words()
scrap.get_english_words()
english_words = scrap.english_words
non_english = scrap.non_english
unique = scrap.get_unique()
print(unique)





"""
user interaction options:
  *1 find the non-English words on the page
  *2 compare to other site(s)
"""

proceedText = "\n\nWant to proceed ;) ? \n Press Y for yes and any other key to exit: "
optionText = "\n Please enter an option\n1. find the non-English words on the page\n2. compare to other site(s) \n"


proceed = input(proceedText)
Urls = []
while(proceed=='Y'):
  print(optionText)
  option = input()
  if option == '1':
    print(non_english)
    proceed = input(proceedText)
  elif option == '2':
    print("Enter Url ( press q to quit):\n")
    url= input()
    while url != 'q':
      Urls.append(url)
      url= input()
    if len(Urls)> 0:
      print(scrap.compare_sites(set(english_words),Urls))
  else: 
    print('\nGoodbye :)')
    proceed = 'N'



  


  







